<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\ReportsController;
use App\Mail\Welcome;

Route::get('/', [HomeController::class, 'index']);

Route::get('/product', [ProductController::class, 'index']);

Route::get('/product/show', [ProductController::class, 'show']);

Route::post('/product', [ProductController::class, 'create']);

Route::get('/product/{id}', [ProductController::class, 'edit']);

Route::put('/product/update/{id}', [ProductController::class, 'update']);

Route::get('/client', [ClientController::class, 'index']);

Route::post('/client', [ClientController::class, 'create']);

Route::post('/sale', [SaleController::class, 'create']);

Route::get('/reports', [ReportsController::class, 'index']);

