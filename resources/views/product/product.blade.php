@extends('layouts.main')

@section('title', 'Produtos')

@section('content')

<div class="container">
    <h3 class="mb-4">Meus produtos</h3>

    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#productModal">
      Novo Produto
    </button>

    @if(count($products) > 0 )
<table id="tableProduct" class="table table-striped table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nome</th>
      <th scope="col">Imagem</th>
      <th scope="col">Quantidade</th>
      <th scope="col">Valor</th>
      <th scope="col">Ações</th>
    </tr>
  </thead>
  <tbody>
  @foreach($products as $product)
    <tr>
      <th scope="row">{{$loop->index + 1 }}</th>
      <td>{{$product->name}}</td>
      <td><img class="image-small-product" src="img/products/{{$product->image}}" class="rounded-circle" alt="{{$product->name}}"></td>
      <td>{{$product->quantity}}</td>
      <td> R$: {{$product->value}}</td>
      <td><a href="/product/{{$product->id}}"><button type="button" class="btn btn-primary btn-sm">Editar</button></a></td>
    </tr>
  @endforeach
  </tbody>
</table>
@else
<div class="m-3">
  <p>Você ainda não tem produtos, esperimente criar alguns.</p>
</div>
@endif
</div>


<!-- Modal -->
<div class="modal fade" id="productModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Novo produto</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form id="formProduct" enctype="multipart/form-data">
          @csrf
          <div class="mb-3">
            <div class="form-group">
              <label class="form-label">Nome do Produto</label>
              <input type="txt" name="product" class="form-control" placeholder="Ex: Alface" required>
            </div>
            <div class="form-group">
              <label for="" class="form-label">Quantidade</label>
              <input type="text" name="quantity" class="form-control"  placeholder="EX: 8" required>
            </div>
            <div class="form-group">
              <label for="" class="form-label">Valor</label>
              <input type="text" name="value" id="" class="form-control" placeholder="Ex: 12,23" required>
            </div>
          <div class="mb-3">
          <label for="formFile" class="form-label">Imagem do produto</label>
          <input class="form-control" type="file" id="image" name="image">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button>
        <button type="submit" class="btn btn-primary" id="productBtn">Salvar</button>
        <button type="submit"id="productLoading" class="btnHide  btn btn-primary" disabled><span class="donutSpinner"></span></button>
      </form>
      </div>
    </div>
  </div>
</div>
<script>
$.ajaxSetup({
       headers: {
           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
       }
   });
   $('#formProduct').submit(function(product){
     product.preventDefault();
     let formData = new FormData(this);
     $('#image').text('');
     $("#productBtn").hide()
     $("#productLoading").show();
     $.ajax({
        type: 'post',
        url: '/product',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function (response){
          if(response.success === true){
            $("#alert").removeClass();
            $("#alert").addClass("alert");
            $("#alert").addClass(" alert-success");
            $("#msnContainer").show('fade'),
            $("#msn").text(response.message);
            $('#tableProduct').load(document.URL +  ' #tableProduct');
            $('#productModal').modal('hide');
            $("#productLoading").hide();
            $("#productBtn").show();
            setTimeout(function() {
             $('#msnContainer').fadeOut('slow');
           }, 3000);
           this.reset();
          }else{
          $("#alert").removeClass();
          $("#alert").addClass("shadow-sm alert");
          $("#alert").addClass("shadow-sm alert-warning");
          $("#msnContainer").show('fade');
          $("#msn").text(response.message);
          $('#productModal').modal('hide');
          $("#productLoading").hide();
          $("#productBtn").show();
            setTimeout(function() {
             $('#msnContainer').fadeOut('slow');
           }, 4000);
           this.reset();
          }

        }
     });
   });

  
</script>
@endsection
