@extends('layouts.main')

@section('title', 'Editar: '.$product->name)

@section('content')

<div class="container">
    <h3>Editando: {{$product->name}}</h3>
</div>

<div class="container">
<div class="card shadow">
<img  src="../img/products/{{$product->image}}" class="float-center rounded img-fluid image-large-product" alt="{{$product->name}}">
  <div class="card-body">
  <form method="post" action="/product/update/{{$product->id}}" id="formProduct" enctype="multipart/form-data">
          @csrf
          @method('PUT')
          <div class="mb-3">
            <div class="form-group">
              <label class="form-label">Nome do Produto</label>
              <input type="txt" name="name" value="{{$product->name}}" class="form-control" placeholder="Ex: Alface" required>
            </div>
            <div class="form-group">
              <label for="" class="form-label">Quantidade</label>
              <input type="text" name="quantity" value="{{$product->quantity}}" class="form-control"  placeholder="EX: 8" required>
            </div>
            <div class="form-group">
              <label for="" class="form-label">Valor</label>
              <input type="text" name="value" value="{{$product->value}}" class="form-control" placeholder="Ex: 12,23" required>
            </div>
            <!--
          <div class="mb-3">
          <label for="formFile" class="form-label">Imagem do produto</label>
          <input class="form-control" type="file" id="image" name="image">
        </div>
        -->
        <div class="mt-3">
            <a href="/product"><button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button></a>
            <button type="submit" class="btn btn-primary" id="productBtn">Salvar</button>
        </div>
    </form>
  </div>
</div>
</dic>
@endsection