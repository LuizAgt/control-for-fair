<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title>@yield('title')</title>
    <!-- Bootstrap CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <script src="../js/theme.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>

</head>
<body class="theme">
<header class="p-3 ">
  <div class="container">
    <div class="d-flex flex-wrap  justify-content-end">

      <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center ">
        <li><a href="/" class="shadow-sm btn btn-secondary btn-sm m-2"><img class="icon-m3" src="../img/icons/home.svg" alt="Home"> Home</a></li>
        <li><a href="/product" class="shadow-sm btn btn-secondary btn-sm m-2"><img class="icon-m3" src="../img/icons/small-product.svg" alt="Produtos"> Produtos</a></li>
        <li><a href="/client" class="shadow-sm btn btn-secondary btn-sm m-2"><img class="icon-m3" src="../img/icons/customer.svg" alt="Clientes"> Clientes</a></li>
        <li><a href="/reports" class="shadow-sm  btn btn-secondary btn-sm m-2"><img class="icon-m3" src="../img/icons/statistics.svg" alt="Relatórios"> Relatórios</a></li>
      </ul>
    </div>
  </div>
</header>
@if(session('msg'))
<div class="container">
  <div class="alert alert-info alert-dismissible fade show" role="alert">
    <strong>{{session('msg')}}</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
  </div>
 </div>
@endif

<div class="container btnHide" id="msnContainer">
  <div id="alert" class="shadow alert" role="alert">
  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-info-circle-fill" viewBox="0 0 16 16">
  <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
</svg>
  <strong id="msn"></strong>
  </div>
 </div>
<div class="container">
 <div class="form-check form-switch float-end">
  <input class="form-check-input" onchange="toggleTheme()" type="checkbox" id="slider">
  <label class="theme-label form-check-label">alterar tema</label>
</div>
</div>

    @yield('content')
</body>


</html>
