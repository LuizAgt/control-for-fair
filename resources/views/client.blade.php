@extends('layouts.main')

@section('title', 'Clientes')

@section('content')


<div id="clients" class="container">
    <h3 class="mb-4">Meus clientes</h3>
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#NewProductModal">

      Novo cliente
    </button>

    @if(count($clients) > 0 )
    <table class="table mt-1">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Telefone</th>
    </tr>
  </thead>
  <tbody>
    @foreach($clients as $client)
    <tr>
      <th scope="row">{{$loop->index + 1 }}</th>
      <td>{{$client->name}}</td>
      <td>{{$client->email}}</td>
      <td>{{$client->phone_number}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
@else
<div class="m-3">
  <p>Você ainda não tem clientes, esperimente criar alguns.</p>
</div>
@endif

</div>

<!-- Modal -->
<div class="modal fade" id="NewProductModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Novo cliente</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form name="clientForm">
          @csrf
          <div class="mb-3">
            <div class="form-group">
              <label class="form-label">Nome do Cliente *</label>
              <input type="txt" name="client" class="form-control" placeholder="Ex: Maria" required>
            </div>
            <div class="form-group">
              <label for="" class="form-label">Numero de telefone *</label>
              <input type="text" name="phone_number" class="form-control"  placeholder="35 999265816" required>
            </div>
            <div class="form-group">
              <label for="" class="form-label">Email *</label>
              <input type="mail" name="email" id="" class="form-control" placeholder="Ex: maria@gmail.com" required>
            </div>
            
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">cancelar</button>
        <button type="submit" id="clientBtn" class="btn btn-primary">Salvar</button>
        <button type="submit"id="clientLoading" class="btnHide  btn btn-primary" disabled><span class="donutSpinner"></span></button>
      </form>
      </div>
    </div>
  </div>
</div>
<!-- / Modal -->
<script>
$(function(){
  $('form[name="clientForm"]').submit(function(client){
    client.preventDefault();

    $(document).ready(function() {
        getData();
      })
      $("#clientBtn").hide()
      $("#clientLoading").show();
    $.ajax({
      url: "/client",
      type: "post",
      data: $(this).serialize(),
      dataType: 'json',
      success: function(response){
        if(response.success === true) {
            //alert('Sucesso ' + );
            $("#alert").removeClass();
            $("#alert").addClass("alert");
            $("#alert").addClass(" alert-success");
            $("#msnContainer").show('fade');
            $("#msn").text(response.message);
            $('#clients').load(document.URL +  ' #clients');
            $('#NewProductModal').modal('hide');
            $("#clientLoading").hide();
            $("#clientBtn").show();
            setTimeout(function() {
             $('#msnContainer').fadeOut('slow');
           }, 3000);
            this.reset();
          }else{
            $("#alert").removeClass();
            $("#alert").addClass("alert");
            $("#alert").addClass("alert-warning");
            $("#msnContainer").show('fade');
            $("#msn").text(response.message);
            $("#clientLoading").hide();
            $("#clientBtn").show();
            setTimeout(function() {
             $('#msnContainer').fadeOut('slow');
           }, 3000);
          }
      }
    })
  });
});
</script>

@endsection
