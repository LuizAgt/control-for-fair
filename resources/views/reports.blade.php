@extends('layouts.main')

@section('title', 'Clientes')

@section('content')

<script src="js/chart.min.js"></script>
<div class="container">
    <h3 class="mb-4">Meus relatórios</h3>
</div>

<div class="container">
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Mais vendidos</button>
    <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Clientes</button>
    <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Estoque</button>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <canvas id="myChartProductSales" width="600" height="600"></canvas>
      <script>
      var ctx = document.getElementById('myChartProductSales').getContext('2d');
      var myChart = new Chart(ctx, {
          type: 'pie',
          data: {
              labels: [
                @if($productsSales->count() > 0 )
                  @foreach($products as $product)
                  '{{$product->name}}',
                  @endforeach
                  @endif

                ],
              datasets: [{
                  label: '# de vendas',
                  data: [
                    @if($products->count() > 0 )
                      @foreach($products as $product)
                        '{{$product->total_sales}}',
                      @endforeach
                    @endif
                  ],
                  backgroundColor: [
                      'rgba(255, 99, 132, 0.2)',
                      'rgba(54, 162, 235, 0.2)',
                      'rgba(255, 206, 86, 0.2)',
                      'rgba(75, 192, 192, 0.2)',
                      'rgba(153, 102, 255, 0.2)',
                      'rgba(255, 159, 64, 0.2)'
                  ],
                  borderColor: [
                      'rgba(255, 99, 132, 1)',
                      'rgba(54, 162, 235, 1)',
                      'rgba(255, 206, 86, 1)',
                      'rgba(75, 192, 192, 1)',
                      'rgba(153, 102, 255, 1)',
                      'rgba(255, 159, 64, 1)'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
            maintainAspectRatio: false,
              scales: {
                  
              }
          }
      });
      </script>
  </div>

  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
    <canvas id="mySales" width="600" height="600"></canvas>
<script>
var ctx = document.getElementById('mySales').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
          @if($clients->count() > 0 )
            @foreach($clients as $client)
            '{{$client->name}}',
            @endforeach
            @endif
        ],
        datasets: [{
            label: '# produtos comprados',
            data: [
              @if($clients->count() > 0 )
                @foreach($clients as $client)
                '{{$client->total_sales_client}}',
                @endforeach
                @endif
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});
</script>
  </div>
  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
    <canvas id="myStock" width="400" height="400"></canvas>
    <script>
    var ctx = document.getElementById('myStock').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
              @if($products->count() > 0 )
                @foreach($productsSales as $productSale)
                '{{$productSale->name}}',
                @endforeach
                @endif
            ],
            datasets: [{
                label: '# em estoque',
                data: [
                  @if($productsSales->count() > 0 )
                    @foreach($productsSales as $productSale)
                    '{{$productSale->quantity}}',
                    @endforeach
                    @endif
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
          maintainAspectRatio: false,
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    </script>
  </div>
</div>
</div>
@endsection
