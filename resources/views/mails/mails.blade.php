@extends('layouts.message')
@section('content')
     <h3><b>Nova venda!</b></h3>
     <hr>
     <p><b>Cliente: </b>{{ $data['cliente'] }}</p>
     <p><b>Produto: </b>{{ $data['produto'] }}</p>
     <p><b>Quantidade: </b>{{ $data['quantidade'] }}</p>
     <p><b>Valor da venda: R$: </b>{{ $data['valor'] }}</p>
@endsection
