@extends('layouts.main')

@section('title', 'Página Principal')

@section('content')
<div class="container">
    <div class="row ">
      <div class="d-flex">
        <!--<img class="icon-m6" src="img/icons/emogi.svg" alt="Olá">-->
        <h1 class="mb-3 m-1"><b> Olá, bem-vindo(a)</b></h1>
      </div>

    </div>
    <div>
      <div id="info-product" class="btnHide">
      <div class="shadow alert-info-product mt-3 mb-3">
      <div class="btn-02">
        <image class="icon-m2" src="img/icons/cost.svg" alt="Custo do produto">
      </div>
      <div>
        <h5 class="m-2">R$: <span id="value"></span></h5>
      </div>
      <div class="btn-02">
        <image class="icon-m2" src="img/icons/stock.svg" alt="Estoque do produto">
      </div>
      <div>
        <h5 class="m-2"><span id="quantity"></span></h5>
      </div>
    </div>
  </div>
    </div>
    <div class="row">
    <div class="col-sm">
      <div class="shadow card mb-3 ">
        <div class="card-header bg-transparent">
          <h3 class="m-2">Nova venda</h3>
        </div>
        <div class="card-body">
          <label>Produto</label>
          <form enctype="multipart/form-data" name="formSale">
          @csrf
          <select name="product" id="product" class="form-select form-control-lg mt-2" required>
          <option value=''></option>
          @if(count($products) > 0 )
          @foreach($products as $products)
          <option value='{{$products->id}}'>{{$products->name}}</option>
          @endforeach
          @endif
          </select>
          <div class="row mt-2">
            <div class="col">
              <label class="mt-2">Cliente</label>
                <select name="client" id="client" class="form-select form-control-lg mt-2" required>
                <option value=''></option>
                @if(count($clients) > 0 ))
                @foreach($clients as $client)
                <option value='{{$client->id}}'>{{$client->name}}</option>
                @endforeach
                @endif
                </select>
            </div>
            <div class="col">
              <label class="mt-2">Quantidade</label>
              <input type="text" name="quantity" id="quantity" class="form-control  mt-2" placeholder="Quantidade do produto" required>
            </div>
          </div>
          <div class="form-check form-switch m-2">
            <input class="form-check-input" type="checkbox" name="checkMail"  id="flexSwitchCheckChecked" checked>
            <label class="form-check-label"  for="flexSwitchCheckChecked">Enviar e-mail da venda</label>
         </div>
          <div class="d-grid gap-2 mt-2">
            <button type="submit" id="saleBtn" class="saleBtn btn btn-primary btn-lg mt-2">Vender</button>
            <button type="submit"id="saleLoading" class="btnHide  btn btn-primary btn-lg mt-2" disabled><span class="donutSpinner"></span></button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col col-lg-3">
      <div class="shadow card card-m1 mb-2">
        <div class="bg-transparent">
          <h3 class="m-3">Suas vendas</h3>
        </div>
        <div id="sales" class="card-body">
        @if($todaySales->count() > 0 )
          @foreach($todaySales as $todaySale)
          <div class="sales-info">
            <div class="btn-03">
             <a href="product"><img class="icon-m4" src="img/icons/calendar-today.svg" alt="Produtos"></a>
            </div>
            <h2 class="m-2">R$: <b>{{$todaySale->sales_today}}</b></h2>
          </div>
          @endforeach
          @else
          <h3>R$: 00,00</h3>
        @endif
          <p>Vendas hoje</p>
          @if($yesterdaySales->count() > 0)
           @foreach($yesterdaySales as $yesterdaySale)
           <div class="sales-info">
            <div class="btn-04">
             <a href="product"><img class="icon-m5" src="img/icons/calendar-yesterday.svg" alt="Produtos"></a>
            </div>
          <h4 class="m-2">R$: <b>{{$yesterdaySale->sales_yesterday}}</b></h4>
          </div>
           @endforeach
           @else
            <h3>R$: 00,00</h3>
          @endif
          <p>Vendas ontem</p>
          <div class="">
            <!--<button type="submit" class="btn btn-primary btn-sm mt-2">Gerar relatório</button>-->
          </div>
        </div>
      </div>
    </div>

</div>


<div class="container mt-3 mb-3">
<h5 class="mb-4">Sua gestão</h5>
<div class="container">
  <div class="row row-cols-auto">
    <div class="col">
    <div class="text-center">
      <a href="product">
        <div class="btn-01">
          <img class="icon-m1" src="img/icons/product.svg" alt="Produtos">
        </div>
        </a>
      <p class="mt-2">Produtos</p>
      </div>
    </div>
    <div class="col">
      <div class="text-center">
      <a href="/client">
        <div class="btn-01">
          <img class="icon-m1" src="img/icons/customer.svg" alt="Clientes">
        </div>
        </a>
      <p class="mt-2">Clientes</p>
      </div>
    </div>
    <div class="col">
    <div class="text-center">
      <a href="/reports">
        <div class="btn-01">
          <img class="icon-m1" src="img/icons/statistics.svg" alt="">
        </div>
      </a>
      <p class="mt-2">Relatórios</p>
      </div>
    </div>
  </div>
</div>
</div>
    <div id="salesProducts" class="container">
      <h5 class="mb-4">Mais vendidos</h5>
      <main>
       <div class="row row-cols-1 row-cols-md-4 mb-3 text-center">
           @if($salesProducts->count()  > 0 )
           @foreach($salesProducts as $salesProduct)
           <div class="col">
             <div class="shadow card m-2 mb-3">
               <div class="">
                 <!--<h2 class="badge rounded-pill bg-info text-dark">#{{$loop->index + 1 }}</h2>-->
                 <h4><span class="float-end m-1 rounded-pill badge bg-secondary">#{{$loop->index + 1 }}</span></h4>
                 <div class="text-center">
                   <img class="image-product text-center img-fluid" src="img/products/{{$salesProduct->image}}" alt="{{$salesProduct->name}}">
                 </div>
                 <h3 class="card-title">{{$salesProduct->name}}</h3>
                  <div class="baseboard-product d-flex justify-content-center">
                    <div class="m-2">
                     <h2><b>{{$salesProduct->total_quantity}}</b></h2>
                     <p>Vendidos</p>
                    </div>
                    <div class="m-2">
                     <h2><b>{{$salesProduct->quantity}}</b></h2>
                     <p>Em estoque</p>
                    </div>
                  </div>
               </div>
             </div>
             </div>
           @endforeach
           @else
           <p>Você ainda não vez nenhuma venda</p>
           @endif
       </div>
     </main>

  </div>
  <script>

  $(function(){
    $('select[name="product"]').change(function(seeProduct){
      $("#info-product").show('fade');
      $(document).ready(function() {
        getData();
      });
      setTimeout(function() {
       $('#info-product').fadeOut('slow');
     }, 9000);
      $.ajax({
        url: "/product/show",
        type: "get",
        data: $(this).serialize(),
        dataType: 'json',
        success: function(response){
          if(response.success === true) {
            $("#value").text(response.seeProduct.value);
            $("#quantity").text(response.seeProduct.quantity);

          }else{

          }
        }
      });
    });


    $('form[name="formSale"]').submit(function(product){
      product.preventDefault();
      $(document).ready(function() {
        getData();
      })
      $("#saleBtn").hide()
      $("#saleLoading").show();
      $.ajax({
        url: "/sale",
        type: "post",
        data: $(this).serialize(),
        dataType: 'json',
        success: function(response){
          if(response.success === true) {
            $("#alert").removeClass();
            $("#alert").addClass("alert");
            $("#alert").addClass(" alert-success");
            $("#msnContainer").show('fade'),
            $("#msn").text(response.message);
            //Atualiza divs
            $('#sales').load(document.URL +  ' #sales');
            $('#salesProducts').load(document.URL + ' #salesProducts');
            //Remover menssagem após 3 segundos
            setTimeout(function() {
             $('#msnContainer').fadeOut('slow');

           }, 3000);
           $("#saleBtn").show();
           $("#saleLoading").hide();
           $("#info-product").hide();
          }else{
            $("#alert").removeClass();
            $("#alert").addClass("shadow-sm alert");
            $("#alert").addClass("shadow-sm alert-warning");
            $("#msnContainer").show('fade');
            $("#msn").text(response.message);
            //Remover menssagem após 3 segundos
            setTimeout(function() {
             $('#msnContainer').fadeOut('slow');
           }, 3000);
           $("#saleBtn").show();
           $("#saleLoading").hide();
           $("#info-product").hide();
          }
          //console.log(response);
        }


      });
      //limpa o formulário
      this.reset();
    });

  });

  </script>

@endsection
