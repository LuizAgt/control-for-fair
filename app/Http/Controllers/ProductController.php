<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Sale;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('product.product', ['products' => $products]);
    }

    public function create(Request $request)
    {
        $product = new Product;

        $product->name = $request->product;
        $product->value = $request->value;
        $product->quantity = $request->quantity;
        //trocar , por .
        $product->value= str_replace(",",".",$product->value);

        //Verifica se a quantidade informada é um número válido
        if ((is_int($product->quantity) || ctype_digit($product->quantity)) && (int)$product->quantity > 0 ){
        //upload image
        if($request->hasFile('image') && $request->file('image')->isValid()){

                $requestImage = $request->image;

                $fileArray = array('image' => $requestImage);
                //Verificar Imagem
                $rules = array(
                  'image' => 'mimes:jpeg,jpg,png|required|max:10000' // max 10000kb
                );

                $validator = Validator::make($fileArray, $rules);

                if ($validator->fails()){
                  $productStatus['success'] = false;
                  $productStatus['message'] = 'Imagem inválida, verifique se é do tipo jpeg, jpg ou png';
                  echo json_encode($productStatus);
                  return;
                }
               
                $extension = $requestImage->extension();

                $imageName = md5($requestImage->getClientOriginalName(). strtotime("now")). "." .$extension;

                $requestImage->move(public_path('img/products'), $imageName);

                $product->image = $imageName;
        }
        $product->save();

        $productStatus['success'] = true;
        $productStatus['message'] = 'Produto cadastrado';
        echo json_encode($productStatus);
        return;

      }else{
        $productStatus['success'] = false;
        $productStatus['message'] = 'A quantidade informada não é válida!';
        echo json_encode($productStatus);
        return;

      }

}
      public function show(Request $request)
    {
      $product = new Product;
      $product->product = $request->product;
      //Buscar o produto
      $searchProduct = DB::table('products')->where('id', $product->product )->first();

      $productStatus['success'] = true;
      $productStatus['seeProduct'] = $searchProduct;
      echo json_encode($productStatus);
      return;


    }

    public function edit($id)
    {
      $product = Product::findOrFail($id);
      return view('product.edit', ['product' => $product]);
    }

    public function update(Request $request)
    {
      $data = $request->all();
      Product::findOrFail($request->id)->update($data);
      return redirect('/product')->with('msg', 'Produto editado com sucesso!');
    }
}
