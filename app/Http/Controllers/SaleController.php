<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale;
use App\Models\Product;
use App\Mail\Welcome;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
class SaleController extends Controller
{
    public function create(Request $request)
    {
        $sale = new Sale;

        $sale->client = $request->client;
        $sale->product = $request->product;
        $sale->quantity = $request->quantity;

        $checkMail = $request->checkMail;
        //Verifica se a quantidade informada é um número válido
        if ((is_int($sale->quantity) || ctype_digit($sale->quantity)) && (int)$sale->quantity > 0 ){
        //Verificar a imagem
        $client = DB::table('clients')->where('id', $sale->client)->first();
        //Buscar o produto
        $product = DB::table('products')->where('id', $sale->product)->first();
        $sale->value = $product->value * $request->quantity;
        //Verificar o estoque
        if($request->quantity > $product->quantity){

            $saleStatus['success'] = false;
            $saleStatus['message'] = 'O estoque desse produto não é suficiente para realizar a venda';
            echo json_encode($saleStatus);
            return;
        }
        //Atualizar o estoque do produto
        $products = Product::find($request->product);
        $products->quantity = $product->quantity - $request->quantity;
        $products->save();
        $sale->save();

        if($checkMail  === 'on'){
          //Enviar email
          $data = array(
              'cliente' => $client->name,
              'produto' => $product->name,
              'quantidade' => $sale->quantity ,
              'valor' => $sale->value
                  );
              Mail::to('luizaugustofernandoguimaraes@gmail.com')->send( new Welcome($data));
        }
        $saleStatus['success'] = true;
        $saleStatus['message'] = 'Venda realizada com sucesso!';
        echo json_encode($saleStatus);
        return;

        }else{
            $saleStatus['success'] = false;
            $saleStatus['message'] = 'A quantidade informada está em um formato inválido ';
            echo json_encode($saleStatus);
            return;
        }
    }
}
