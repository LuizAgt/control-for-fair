<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;

class ClientController extends Controller
{
    public function index()
    {
        $clients = Client::all();
        return view('client', ['clients' => $clients]);
    }

    public function create(Request $request)
    {
        $client = new Client;

        $client->name = $request->client;
        $client->email =  $request->email;
        $client->phone_number = $request->phone_number;
        $client->save();

        //return redirect('/client')->with('msg', 'Cliente cadastrado com sucesso!');
        $clientStatus['success'] = true;
        $clientStatus['message'] = 'Cliente cadastrado com sucesso!';
        echo json_encode($clientStatus);
        return;
    }
}
