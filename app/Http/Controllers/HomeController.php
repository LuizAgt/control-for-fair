<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Client;
use App\Models\Sale;
use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $clients = Client::all();

        $today = date("Y-m-d", strtotime( '0 days' ) );
        $yesterday = date("Y-m-d", strtotime( '-1 days' ) );


        //Vendas de ontem
        $yesterdaySales = DB::table('sales')
                      ->select('sales.id', DB::raw('SUM(sales.value) as sales_yesterday'))
                      ->whereDate('created_at', $yesterday )
                      ->get();

      //Vendas de hoje
      $todaySales = DB::table('sales')
                    ->select('sales.id', DB::raw('SUM(sales.value) as sales_today'))
                    ->whereDate('created_at', $today )
                    ->get();
        //4 produtos mais vendidos
        $salesProducts = DB::table('sales')
                    ->select('products.name', 'products.image', 'products.quantity', DB::raw('SUM(sales.quantity) as total_quantity'))
                    ->join('products', 'sales.product', '=', 'products.id')
                    ->groupBy('products.name')
                    ->orderBy('total_quantity', 'DESC')
                    ->limit(4)
                    ->get();

        return view('home', ['products' => $products, 'clients' => $clients, 'todaySales' => $todaySales, 'yesterdaySales' => $yesterdaySales, 'salesProducts' => $salesProducts]);
    }
}
