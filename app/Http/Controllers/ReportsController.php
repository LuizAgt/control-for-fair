<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Client;
use App\Models\Sale;
use Illuminate\Support\Facades\DB;
class ReportsController extends Controller
{
    public function index()
    {

      //Produtos mais vendidos
      $salesProducts = DB::table('sales')
                  ->select('products.name', 'products.image', 'products.quantity', DB::raw('SUM(sales.quantity) as total_sales'))
                  ->join('products', 'sales.product', '=', 'products.id')
                  ->groupBy('products.name')
                  ->orderBy('total_sales', 'DESC')
                  ->get();

      //Compra e cliente
      $clients = DB::table('sales')
                ->select('clients.name', DB::raw('SUM(sales.quantity) as total_sales_client'))
                ->join('clients', 'sales.client', '=', 'clients.id')
                ->groupBy('clients.name')
                ->orderBy('total_sales_client', 'DESC')
                ->get();
      //Todos os produtos
      $products = Product::all();

      return view('reports', ['products' => $salesProducts, 'clients' => $clients, 'productsSales' => $products]);

    }

}
